package rs.ac.singidunum.novisad.primer4;

import java.util.ArrayList;

import rs.ac.singidunum.novisad.primer4.model.Nastavnik;
import rs.ac.singidunum.novisad.primer4.model.Osoba;
import rs.ac.singidunum.novisad.primer4.model.Predmet;
import rs.ac.singidunum.novisad.primer4.model.Prikazivo;
import rs.ac.singidunum.novisad.primer4.model.Radnik;
import rs.ac.singidunum.novisad.primer4.model.Sluzbenik;
import rs.ac.singidunum.novisad.primer4.model.Student;

public class App {
	public static void main(String[] args) {
		Osoba osoba = new Osoba("Marko", "Marković");
		Osoba student = new Student("Petar", "Petrović", "2021123456");
		
		ArrayList<Radnik> radnici = new ArrayList<Radnik>();
		radnici.add(new Nastavnik("Miodrag", "Živković", "P001", "Profesor"));
		radnici.add(new Sluzbenik("Marija", "Marijanović", "S001"));
		
		Predmet predmet = new Predmet("OOP2", "Objektno orijentisano programiranje 2");
		
		ArrayList<Prikazivo> prikaziviObjekti = new ArrayList<>();
		prikaziviObjekti.add(osoba);
		prikaziviObjekti.add(student);
		prikaziviObjekti.addAll(radnici);
		prikaziviObjekti.add(predmet);
		
		for(Prikazivo p : prikaziviObjekti) {
			p.prikaz();
		}
	}
}
