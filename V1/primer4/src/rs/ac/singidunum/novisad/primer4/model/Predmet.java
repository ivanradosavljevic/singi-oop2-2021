package rs.ac.singidunum.novisad.primer4.model;

public class Predmet implements Prikazivo {
	private String sifraPredmeta;
	private String naziv;
	
	public Predmet() {
		super();
	}
	public Predmet(String sifraPredmeta, String naziv) {
		super();
		this.sifraPredmeta = sifraPredmeta;
		this.naziv = naziv;
	}
	public String getSifraPredmeta() {
		return sifraPredmeta;
	}
	public void setSifraPredmeta(String sifraPredmeta) {
		this.sifraPredmeta = sifraPredmeta;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	@Override
	public void prikaz(String krajReda) {
		System.out.print(this.sifraPredmeta + ", " + this.naziv + krajReda);
	}
	@Override
	public void prikaz() {
		this.prikaz("\n");
	}
}
