# Primer 4

1. Definisati klasu Predmet. Predmet je opisan atributima sifraPredmeta i naziv. U ovoj klasi definisati metodu prikaz() koja vrši ispis vrednosti atributa sifraPredmeta i naziv.
2. Instancirati predmet i pozvati metodu prikaz().
3. Napraviti kolekciju koja u sebi sadrži Osobe, Studente, Nastavnike i Predmete. For petljom proći kroz elemente kolekcije i za svaki element pozvati metodu prikaz().