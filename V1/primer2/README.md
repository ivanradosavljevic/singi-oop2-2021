# Primer 2

1. Dodati klasu Osoba, ova klasa sadrži atribute ime i prezime. 
2. Prepraviti klase Student i Nastavnik tako da nasleđuju klasu osoba.
3. Implementirati metodu prikaz() u klasi Osoba.
4. Upotrebom metode prikaz() iz klase Osoba definisati metodu prikaz() u klasama Student i Nastavnik.
5. U main metodi instancirati klase Osoba, Student i Nastavnik i testirati metodu prikaz.