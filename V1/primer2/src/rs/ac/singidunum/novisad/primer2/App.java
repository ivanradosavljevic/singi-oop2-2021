package rs.ac.singidunum.novisad.primer2;

import rs.ac.singidunum.novisad.primer2.model.Nastavnik;
import rs.ac.singidunum.novisad.primer2.model.Osoba;
import rs.ac.singidunum.novisad.primer2.model.Student;

public class App {
	public static void main(String[] args) {
		Osoba osoba = new Osoba("Marko", "Marković");
		Osoba nastavnik = new Nastavnik("Miodrag", "Živković", "Profesor");
		Osoba student = new Student("Petar", "Petrović", "2021123456");
		
		osoba.prikaz();
		nastavnik.prikaz();
		student.prikaz();
	}
}
