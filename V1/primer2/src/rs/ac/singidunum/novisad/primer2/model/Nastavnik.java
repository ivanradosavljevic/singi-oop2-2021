package rs.ac.singidunum.novisad.primer2.model;

public class Nastavnik extends Osoba {
	private String zvanje;
	
	public Nastavnik() {
		super();
	}
	public Nastavnik(String ime, String prezime, String zvanje) {
		super(ime, prezime);
		this.zvanje = zvanje;
	}
	public String getZvanje() {
		return zvanje;
	}
	public void setZvanje(String zvanje) {
		this.zvanje = zvanje;
	}
	@Override
	public void prikaz() {
		super.prikaz("");
		System.out.println(", " + this.zvanje);
	}
}
