# Primer 3

1. Definisati apstraktnu klasu Radnik, ova klasa nasleđuje klasu Osoba i dodatno sadrži atribut sifraZaposlenog. U ovoj klasi redefinisati metodu prikaz() iz klase Osoba tako da se prvo ispisuje vrednost atributa sifraZaposlenog a potom ime i prezime.
2. Prepraviti klasu Nastavnik tako da nasleđuje klasu Radnik.
3. Dodati klasu Sluzbenik koja nasleđuje klasu Radnik. Ova klasa nema dodatne atribute ili metode.
4. U main metodi instancirati sve navedene klase i nad njima pozvati metodu prikaz().
5. Prilikom instanciranja radnika, sve radnike smestiti u kolekciju radnika. For petljom proći kroz kolekciju radnika i za svakog radnika pozvati metodu prikaz().