package ac.rs.singidunum.novisad.primer3.model;

public class Student extends Osoba {
	private String brojIndeksa;
	
	public Student() {
		super();
	}
	public Student(String ime, String prezime, String brojIndeksa) {
		super(ime, prezime);
		this.brojIndeksa = brojIndeksa;
	}
	public String getBrojIndeksa() {
		return brojIndeksa;
	}
	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}
	@Override
	public void prikaz() {
		System.out.print(this.brojIndeksa + ", ");
		super.prikaz();
	}

}
