package rs.ac.singidunum.novisad.primer1;

import rs.ac.singidunum.novisad.primer1.model.Nastavnik;
import rs.ac.singidunum.novisad.primer1.model.Student;

public class App {
	public static void main(String[] args) {
		Nastavnik nastavnik = new Nastavnik("Miodrag", "Živković", "Profesor");
		Student student = new Student("Petar", "Petrović", "2021123456");
		
		nastavnik.prikaz();
		student.prikaz();
	}
}
