package rs.ac.singidunum.novisad.primer1.model;

public class Student {
	private String ime;
	private String prezime;
	private String brojIndeksa;
	
	public Student() {
		super();
	}
	public Student(String ime, String prezime, String brojIndeksa) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.brojIndeksa = brojIndeksa;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getBrojIndeksa() {
		return brojIndeksa;
	}
	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}
	
	public void prikaz() {
		System.out.println(this.brojIndeksa + ", " + this.ime + " " + this.prezime);
	}
}
