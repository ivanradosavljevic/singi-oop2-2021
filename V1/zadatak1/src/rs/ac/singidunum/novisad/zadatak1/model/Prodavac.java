package rs.ac.singidunum.novisad.zadatak1.model;

public class Prodavac extends Zaposleni {
	public Prodavac() {
		super();
	}

	public Prodavac(String ime, String prezime, String sifraZaposlenig) {
		super(ime, prezime, sifraZaposlenig);
	}
}
