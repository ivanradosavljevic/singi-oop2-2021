package rs.ac.singidunum.novisad.zadatak1.model;

public interface Prikazivo {
	public void prikaz();
	public void prikaz(String krajReda);
}
