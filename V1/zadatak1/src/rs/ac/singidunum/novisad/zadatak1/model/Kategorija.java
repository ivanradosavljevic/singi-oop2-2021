package rs.ac.singidunum.novisad.zadatak1.model;

public class Kategorija implements Prikazivo {
	private String naziv;

	public Kategorija() {
		super();
	}

	public Kategorija(String naziv) {
		super();
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public void prikaz() {
		this.prikaz("\n");
	}

	@Override
	public void prikaz(String krajReda) {
		System.out.print(naziv + krajReda);
	}
}
