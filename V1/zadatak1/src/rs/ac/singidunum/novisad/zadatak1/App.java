package rs.ac.singidunum.novisad.zadatak1;

import java.util.ArrayList;

import rs.ac.singidunum.novisad.zadatak1.model.Kategorija;
import rs.ac.singidunum.novisad.zadatak1.model.Prodavac;
import rs.ac.singidunum.novisad.zadatak1.model.Prodavnica;
import rs.ac.singidunum.novisad.zadatak1.model.Racun;
import rs.ac.singidunum.novisad.zadatak1.model.Sporet;
import rs.ac.singidunum.novisad.zadatak1.model.TehnickiProizvod;
import rs.ac.singidunum.novisad.zadatak1.model.Televizor;
import rs.ac.singidunum.novisad.zadatak1.model.Zaposleni;

public class App {

	public static void main(String[] args) {
		ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>();
		kategorije.add(new Kategorija("Televizori"));
		kategorije.add(new Kategorija("Šporeti"));

		for (Kategorija k : kategorije) {
			k.prikaz();
		}

		ArrayList<Zaposleni> zaposleni = new ArrayList<Zaposleni>();
		zaposleni.add(new Prodavac("Marko", "Marković", "P001"));
		zaposleni.add(new Prodavac("Marija", "Marković", "P002"));

		for (Zaposleni z : zaposleni) {
			z.prikaz();
		}

		ArrayList<TehnickiProizvod> tehnickiPorizvodi = new ArrayList<TehnickiProizvod>();
		tehnickiPorizvodi.add(new Televizor("LG", "55NANO753PR", kategorije.get(0), 80000, 55.0));
		tehnickiPorizvodi.add(new Sporet("Bosch", "HKR39A150", kategorije.get(1), 74000, 4, false, "Bela", 50, 70, 70));
		
		for (TehnickiProizvod tp : tehnickiPorizvodi) {
			tp.prikaz();
		}

		ArrayList<Racun> racuni = new ArrayList<Racun>();
		racuni.add(new Racun((Prodavac)zaposleni.get(0), tehnickiPorizvodi));
		
		for(Racun r : racuni) {
			r.prikaz();
		}
		
		Prodavnica prodavnica = new Prodavnica(zaposleni, tehnickiPorizvodi);
		prodavnica.ukloni(2);
		prodavnica.dodaj(new Televizor("LG", "55NANO753PR", kategorije.get(0), 95000, 55.0));
		prodavnica.prikaz();
		
		prodavnica.izdajRacun(1, 0).prikaz();;
	}

}
