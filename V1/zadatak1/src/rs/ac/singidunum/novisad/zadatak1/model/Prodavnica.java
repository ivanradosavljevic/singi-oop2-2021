package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;

public class Prodavnica implements Prikazivo, Kolekcija {
	private ArrayList<Zaposleni> zaposleni = new ArrayList<Zaposleni>();
	private ArrayList<TehnickiProizvod> tehnickiProizvodi = new ArrayList<TehnickiProizvod>();

	public Prodavnica() {
		super();
	}

	public Prodavnica(ArrayList<Zaposleni> zaposleni, ArrayList<TehnickiProizvod> tehnickiProizvodi) {
		super();
		this.zaposleni = zaposleni;
		this.tehnickiProizvodi = tehnickiProizvodi;
	}

	public ArrayList<Zaposleni> getZaposleni() {
		return zaposleni;
	}

	public void setZaposleni(ArrayList<Zaposleni> zaposleni) {
		this.zaposleni = zaposleni;
	}

	public ArrayList<TehnickiProizvod> getTehnickiProizvodi() {
		return tehnickiProizvodi;
	}

	public void setTehnickiProizvodi(ArrayList<TehnickiProizvod> tehnickiProizvodi) {
		this.tehnickiProizvodi = tehnickiProizvodi;
	}

	public void dodajZaposlenog(Zaposleni zaposleni) {
		this.zaposleni.add(zaposleni);
	}

	public Zaposleni dobaviZaposlenog(int indeks) {
		return this.zaposleni.get(indeks);
	}

	public void ukloniZaposlenig(int indeks) {
		this.zaposleni.remove(indeks);
	}

	public TehnickiProizvod dobaviTehnickiProizvod(int indeks) {
		return this.tehnickiProizvodi.get(indeks);
	}

	public void dodajTehnickiProizvod(TehnickiProizvod tehnickiProizvod) {
		this.tehnickiProizvodi.add(tehnickiProizvod);
	}

	public void ukloniTehnickiProizvod(int indeks) {
		this.tehnickiProizvodi.remove(indeks);
	}

	public Racun izdajRacun(int indeksProdavca, int... indeksiTehnickihProizvoda) {
		Zaposleni zaposleni = this.dobaviZaposlenog(indeksProdavca);
		if (!(zaposleni instanceof Prodavac)) {
			return null;
		}

		Racun r = new Racun((Prodavac) zaposleni, new ArrayList<TehnickiProizvod>());
		
		for (int i : indeksiTehnickihProizvoda) {
			r.dodaj(this.dobaviTehnickiProizvod(i));
		}

		return r;
	}

	@Override
	public void prikaz() {
		this.prikaz("");
	}

	@Override
	public void prikaz(String krajReda) {
		System.out.println("Zaposleni: ");
		for (Zaposleni z : zaposleni) {
			z.prikaz();
		}
		System.out.println("Proizvodi: ");
		for (TehnickiProizvod tp : tehnickiProizvodi) {
			tp.prikaz();
		}
		System.out.print(krajReda);
	}

	@Override
	public Object dobavi(int indeks) {
		if (indeks < this.zaposleni.size()) {
			return this.dobaviZaposlenog(indeks);
		}
		return this.dobaviTehnickiProizvod(indeks - this.zaposleni.size());
	}

	@Override
	public void dodaj(Object o) {
		if (o instanceof TehnickiProizvod) {
			this.dodajTehnickiProizvod((TehnickiProizvod) o);
		} else if (o instanceof Zaposleni) {
			this.dodajZaposlenog((Zaposleni) o);
		}
	}

	@Override
	public void ukloni(int indeks) {
		if (indeks < this.zaposleni.size()) {
			this.ukloniZaposlenig(indeks);
		}
		this.ukloniTehnickiProizvod(indeks - this.zaposleni.size());
	}

}
