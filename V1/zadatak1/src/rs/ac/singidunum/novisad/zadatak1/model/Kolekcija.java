package rs.ac.singidunum.novisad.zadatak1.model;

public interface Kolekcija {
	Object dobavi(int indeks);
	void dodaj(Object o);
	void ukloni(int indeks);	
}
