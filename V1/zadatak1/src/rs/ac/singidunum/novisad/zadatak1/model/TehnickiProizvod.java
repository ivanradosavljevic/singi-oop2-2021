package rs.ac.singidunum.novisad.zadatak1.model;

public abstract class TehnickiProizvod implements Prikazivo {
	protected String marka;
	protected String model;
	protected Kategorija kategorija;
	protected double cena;

	public TehnickiProizvod() {
		super();
	}

	public TehnickiProizvod(String marka, String model, Kategorija kategorija, double cena) {
		super();
		this.marka = marka;
		this.model = model;
		this.kategorija = kategorija;
		this.cena = cena;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	@Override
	public void prikaz() {
		this.prikaz("\n");
	}

	@Override
	public void prikaz(String krajReda) {
		this.kategorija.prikaz(" ");
		System.out.print(this.getMarka() + " " + this.model + " " + this.cena + krajReda);

	}
}
