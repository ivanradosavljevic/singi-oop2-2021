package rs.ac.singidunum.novisad.zadatak1.model;

public class Televizor extends TehnickiProizvod {
	private double dijagonala;

	public Televizor() {
		super();
	}

	public Televizor(String marka, String model, Kategorija kategorija, double cena, double dijagonala) {
		super(marka, model, kategorija, cena);
		this.dijagonala = dijagonala;
	}

	public double getDijagonala() {
		return dijagonala;
	}

	public void setDijagonala(double dijagonala) {
		this.dijagonala = dijagonala;
	}

	@Override
	public void prikaz(String krajReda) {
		super.prikaz(" ");
		System.out.print(this.dijagonala + "\"" + krajReda);
	}
}
