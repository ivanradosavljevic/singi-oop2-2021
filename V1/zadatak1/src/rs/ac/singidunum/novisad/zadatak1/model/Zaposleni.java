package rs.ac.singidunum.novisad.zadatak1.model;

public abstract class Zaposleni extends Osoba {
	protected String sifraZaposlenig;

	public Zaposleni() {
		super();
	}

	public Zaposleni(String ime, String prezime, String sifraZaposlenig) {
		super(ime, prezime);
		this.sifraZaposlenig = sifraZaposlenig;
	}

	public String getSifraZaposlenig() {
		return sifraZaposlenig;
	}

	public void setSifraZaposlenig(String sifraZaposlenig) {
		this.sifraZaposlenig = sifraZaposlenig;
	}

	@Override
	public void prikaz(String krajReda) {
		System.out.print(this.sifraZaposlenig + " ");
		super.prikaz(krajReda);
	}
}
