package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;

public class Racun implements Prikazivo, Kolekcija {
	private Prodavac prodavac;
	private ArrayList<TehnickiProizvod> proizvodi = new ArrayList<TehnickiProizvod>();

	public Racun() {
		super();
	}

	public Racun(Prodavac prodavac, ArrayList<TehnickiProizvod> proizvodi) {
		super();
		this.prodavac = prodavac;
		this.proizvodi = proizvodi;
	}

	private double getCena() {
		double cena = 0;
		for (TehnickiProizvod p : proizvodi) {
			cena += p.getCena();
		}
		return cena;
	}

	@Override
	public Object dobavi(int indeks) {
		return this.proizvodi.get(indeks);
	}

	@Override
	public void dodaj(Object o) {
		if (o instanceof TehnickiProizvod) {
			this.proizvodi.add((TehnickiProizvod) o);
		}
	}

	@Override
	public void ukloni(int indeks) {
		this.proizvodi.remove(indeks);
	}

	@Override
	public void prikaz() {
		this.prikaz("\n");
	}

	@Override
	public void prikaz(String krajReda) {
		System.out.println("========================");
		System.out.print("Racun izdao/la: ");
		this.prodavac.prikaz();
		System.out.println("------------------------");
		for (TehnickiProizvod p : proizvodi) {
			p.prikaz();
		}
		System.out.println("------------------------");
		System.out.println("Ukupno: " + this.getCena());
		System.out.print("========================" + krajReda);
	}
}
