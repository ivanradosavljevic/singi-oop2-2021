package rs.ac.singidunum.novisad.primer1;

import java.util.stream.DoubleStream;

public class App {

	public static void main(String[] args) {
		DoubleStream ds = DoubleStream.of(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0);
		double suma = ds.filter(x -> x % 2 == 0).map(x -> x * x).sum();
		System.out.println(suma);
	}

}
