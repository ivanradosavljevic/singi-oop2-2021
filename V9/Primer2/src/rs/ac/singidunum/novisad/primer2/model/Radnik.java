package rs.ac.singidunum.novisad.primer2.model;

public class Radnik {
	private String ime;
	private String prezime;
	private double visinaPlate;
	private String zaduzenje;

	public Radnik(String ime, String prezime, double visinaPlate, String zaduzenje) {
		this.ime = ime;
		this.prezime = prezime;
		this.visinaPlate = visinaPlate;
		this.zaduzenje = zaduzenje;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public double getVisinaPlate() {
		return visinaPlate;
	}

	public void setVisinaPlate(double visinaPlate) {
		this.visinaPlate = visinaPlate;
	}

	public String getZaduzenje() {
		return zaduzenje;
	}

	public void setZaduzenje(String zaduzenje) {
		this.zaduzenje = zaduzenje;
	}

	@Override
	public String toString() {
		return "Radnik [ime=" + ime + ", prezime=" + prezime + ", visinaPlate=" + visinaPlate + ", zaduzenje="
				+ zaduzenje + "]";
	}
}
