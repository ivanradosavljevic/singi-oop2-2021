package rs.ac.singidunum.novisad.primer5.model;

public class Ocena {
	private String naziv;
	private double ocena;
	private double espb;

	public Ocena(String naziv, double ocena, double espb) {
		this.naziv = naziv;
		this.ocena = ocena;
		this.espb = espb;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getOcena() {
		return ocena;
	}

	public void setOcena(double ocena) {
		this.ocena = ocena;
	}

	public double getEspb() {
		return espb;
	}

	public void setEspb(double espb) {
		this.espb = espb;
	}

	@Override
	public String toString() {
		return "Ocena [naziv=" + naziv + ", ocena=" + ocena + ", espb=" + espb + "]";
	}
}
