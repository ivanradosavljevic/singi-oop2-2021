package rs.ac.singidunum.novisad.primer2;

import rs.ac.singidunum.novisad.primer2.model.Katalog;
import rs.ac.singidunum.novisad.primer2.model.Proizvod;

public class App {

	public static void main(String[] args) {
		Proizvod p1 = new Proizvod("Proizvod 1", 2000, null);
		Proizvod p2 = new Proizvod("Proizvod 2", 55000, 0.05);
		Proizvod p3 = new Proizvod("Proizvod 3", 2400, null);
		Proizvod p4 = new Proizvod("Proizvod 4", 150, 0.1);
		Proizvod p5 = new Proizvod("Proizvod 5", 1500, null);
		
		Katalog k = new Katalog();
		k.dodajProizvod(p1);
		k.dodajProizvod(p2);
		k.dodajProizvod(p3);
		k.dodajProizvod(p4);
		k.dodajProizvod(p5);
		
		Katalog k2 = k.mapiraj(p -> {
			if(p.getPopust() != null) {
				return new Proizvod(p.getNaziv(), p.getCena()*(1-p.getPopust()), null);
			}
			return new Proizvod(p.getNaziv(), p.getCena(), p.getPopust());
		});
		
		System.out.println(k);
		System.out.println(k2);
	}

}
