package rs.ac.singidunum.novisad.primer2.model;

public interface Mapiranje<T> {
	T mapriaj(T o);
}
