package rs.ac.singidunum.novisad.primer2.model;

import java.util.ArrayList;

public class Katalog {
	private ArrayList<Proizvod> proizvodi = new ArrayList<Proizvod>();

	public Katalog() {
		super();
	}

	public ArrayList<Proizvod> getProizvodi() {
		return proizvodi;
	}

	public void setProizvodi(ArrayList<Proizvod> proizvodi) {
		this.proizvodi = proizvodi;
	}

	@Override
	public String toString() {
		String katalogStr = "";
		for(Proizvod p : this.proizvodi) {
			katalogStr += p + "\n";
		}
		return katalogStr;
	}

	public void dodajProizvod(Proizvod p) {
		this.proizvodi.add(p);
	}

	public void ukloniProizvod(Proizvod p) {
		this.proizvodi.remove(p);
	}

	public void ukloniProizvod(int indeks) {
		this.proizvodi.remove(indeks);
	}

	public Katalog mapiraj(Mapiranje<Proizvod> m) {
		Katalog k = new Katalog();
		for (Proizvod p : this.proizvodi) {
			k.dodajProizvod(m.mapriaj(p));
		}
		return k;
	}
}
