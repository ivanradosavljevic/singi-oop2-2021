package rs.ac.singidunum.novisad.primer2.model;

public class Proizvod {
	private String naziv;
	private double cena;
	private Double popust;

	public Proizvod(String naziv, double cena, Double popust) {
		this.naziv = naziv;
		this.cena = cena;
		this.popust = popust;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public Double getPopust() {
		return popust;
	}

	public void setPopust(Double popust) {
		this.popust = popust;
	}

	@Override
	public String toString() {
		return "Proizvod [naziv=" + naziv + ", cena=" + cena + ", popust=" + popust + "]";
	}
}
