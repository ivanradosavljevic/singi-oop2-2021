package rs.ac.singidunum.novisad.primer1.model;

public class Radnik {
	private String ime;
	private String prezime;
	private double visinaPlate;

	public Radnik(String ime, String prezime, double visinaPLate) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.visinaPlate = visinaPLate;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public double getVisinaPlate() {
		return visinaPlate;
	}

	public void setVisinaPlate(double visinaPlate) {
		this.visinaPlate = visinaPlate;
	}

	@Override
	public String toString() {
		return "Radnik [ime=" + ime + ", prezime=" + prezime + ", visinaPlate=" + visinaPlate + "]";
	}
}
