package rs.ac.singidunum.novisad.primer1.model;

public interface Filter<T> {
	boolean filtriraj(T o);
}
