package rs.ac.singidunum.novisad.primer1;

import rs.ac.singidunum.novisad.primer1.model.Filter;
import rs.ac.singidunum.novisad.primer1.model.PlataFilter;
import rs.ac.singidunum.novisad.primer1.model.PlatnaLista;
import rs.ac.singidunum.novisad.primer1.model.Radnik;

public class App {

	public static void main(String[] args) {
		Radnik r1 = new Radnik("Pera", "Markovic", 55000);
		Radnik r2 = new Radnik("Marko", "Jovanivic", 64000);
		Radnik r3 = new Radnik("Jovana", "Peric", 45000);

		PlatnaLista pl = new PlatnaLista();
		pl.dodajRadnika(r1);
		pl.dodajRadnika(r2);
		pl.dodajRadnika(r3);
		
		PlatnaLista novaPl1 = pl.filtriraj(new PlataFilter());
		
		PlatnaLista novaPl2 = pl.filtriraj(new Filter<Radnik>() {
			@Override
			public boolean filtriraj(Radnik r) {
				if (r.getVisinaPlate() < 50000) {
					return true;
				}
				return false;
			}
		});
		
		PlatnaLista novaPl3 = pl.filtriraj(r -> {
			if (r.getVisinaPlate() < 50000) {
				return true;
			}
			return false;
		});
		
		System.out.println(pl);
		System.out.println(novaPl1);
		System.out.println(novaPl2);
		System.out.println(novaPl3);
		
		// reduce
		// compare - sort
	}

}
