package rs.ac.singidunum.novisad.primer1.model;

public class PlataFilter implements Filter<Radnik> {
	@Override
	public boolean filtriraj(Radnik r) {
		if (r.getVisinaPlate() < 50000) {
			return true;
		}
		return false;
	}
}
