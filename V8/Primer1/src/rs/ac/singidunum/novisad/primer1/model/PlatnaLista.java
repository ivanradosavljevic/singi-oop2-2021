package rs.ac.singidunum.novisad.primer1.model;

import java.util.ArrayList;

public class PlatnaLista {
	private ArrayList<Radnik> radnici = new ArrayList<Radnik>();

	public PlatnaLista() {
		super();
	}

	public ArrayList<Radnik> getRadnici() {
		return radnici;
	}

	public void setRadnici(ArrayList<Radnik> radnici) {
		this.radnici = radnici;
	}

	@Override
	public String toString() {
		String radniciStr = "";
		for (Radnik r : radnici) {
			radniciStr += r + "\n";
		}
		return radniciStr;
	}
	
	public void dodajRadnika(Radnik r) {
		this.radnici.add(r);
	}
	
	public void ukloniRadnika(Radnik r) {
		this.radnici.remove(r);
	}
	
	public void ukloniRadnika(int indeks) {
		this.radnici.remove(indeks);
	}
	
	public PlatnaLista filtriraj(Filter<Radnik> filter) {
		PlatnaLista pl = new PlatnaLista();

		for (Radnik r : radnici) {
			if (filter.filtriraj(r)) {
				pl.radnici.add(r);
			}
		}

		return pl;
	}
}
