package rs.ac.singidunum.novisad.primer5.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class GenerickaKolekcija<T> {
	private ArrayList<T> elememnti = new ArrayList<>();

	public GenerickaKolekcija() {
		super();
	}

	public ArrayList<T> getElememnti() {
		return elememnti;
	}

	public void setElememnti(ArrayList<T> elememnti) {
		this.elememnti = elememnti;
	}

	@Override
	public String toString() {
		return "GenerickaKolekcija [elememnti=" + elememnti + "]";
	}

	public void dodajElement(T element) {
		this.elememnti.add(element);
	}

	public void ukloniElement(T element) {
		this.elememnti.remove(element);
	}

	public void ukloniElement(int indeks) {
		this.elememnti.remove(indeks);
	}

	public GenerickaKolekcija<T> filtriraj(Predicate<T> predikat) {
		GenerickaKolekcija<T> kolekcija = new GenerickaKolekcija<>();
		for (T element : this.elememnti) {
			if (predikat.test(element)) {
				kolekcija.dodajElement(element);
			}
		}
		return kolekcija;
	}

	public GenerickaKolekcija<T> mapiraj(Function<T, T> mapiranje) {
		GenerickaKolekcija<T> kolekcija = new GenerickaKolekcija<>();
		for (T element : this.elememnti) {
			kolekcija.dodajElement(mapiranje.apply(element));
		}
		return kolekcija;
	}
	
	public T redukj(BiFunction<T, T, T> redukcija) {
		T akumulator = null;
		if(this.elememnti.size() > 0) {
			akumulator = this.elememnti.get(0);
			
			for(int i = 1; i < this.elememnti.size(); i++) {
				akumulator = redukcija.apply(akumulator, this.elememnti.get(i));
			}
		}
		
		return akumulator;
	}
	
	public GenerickaKolekcija<T> sortiraj(Comparator<T> komparator) {
		GenerickaKolekcija<T> kolekcija = new GenerickaKolekcija<>();
		kolekcija.setElememnti(new ArrayList<T>(this.elememnti));
		
		for(int i = 0; i < kolekcija.elememnti.size(); i++) {
			for(int j = 0; j < kolekcija.elememnti.size()-1; j++) {
				
				if(komparator.compare(kolekcija.elememnti.get(j), kolekcija.elememnti.get(j+1)) > 0) {
					T tmp = kolekcija.elememnti.get(j);
					kolekcija.elememnti.set(j, kolekcija.elememnti.get(j+1));
					kolekcija.elememnti.set(j+1, tmp);
				}
			}
		}
		
		return kolekcija;
	}
}
