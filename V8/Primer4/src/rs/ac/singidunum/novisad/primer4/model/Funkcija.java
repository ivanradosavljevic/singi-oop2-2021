package rs.ac.singidunum.novisad.primer4.model;

public interface Funkcija<T, R> {
	R primeni(T o);
}
