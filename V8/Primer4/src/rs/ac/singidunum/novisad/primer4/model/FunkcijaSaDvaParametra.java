package rs.ac.singidunum.novisad.primer4.model;

public interface FunkcijaSaDvaParametra<T, U, R> {
	R primeni(T o1, U o2);
}
