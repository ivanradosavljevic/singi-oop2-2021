package rs.ac.singidunum.novisad.primer3.model;

import java.util.ArrayList;

public class GenerickaKolekcija<T> {
	private ArrayList<T> elememnti = new ArrayList<>();

	public GenerickaKolekcija() {
		super();
	}

	public ArrayList<T> getElememnti() {
		return elememnti;
	}

	public void setElememnti(ArrayList<T> elememnti) {
		this.elememnti = elememnti;
	}

	@Override
	public String toString() {
		return "GenerickaKolekcija [elememnti=" + elememnti + "]";
	}

	public void dodajElement(T element) {
		this.elememnti.add(element);
	}

	public void ukloniElement(T element) {
		this.elememnti.remove(element);
	}

	public void ukloniElement(int indeks) {
		this.elememnti.remove(indeks);
	}

	public GenerickaKolekcija<T> filtriraj(Filter<T> predikat) {
		GenerickaKolekcija<T> kolekcija = new GenerickaKolekcija<>();
		for (T element : this.elememnti) {
			if (predikat.filtriraj(element)) {
				kolekcija.dodajElement(element);
			}
		}
		return kolekcija;
	}

	public GenerickaKolekcija<T> mapiraj(Mapiranje<T> mapiranje) {
		GenerickaKolekcija<T> kolekcija = new GenerickaKolekcija<>();
		for (T element : this.elememnti) {
			kolekcija.dodajElement(mapiranje.mapiraj(element));
		}
		return kolekcija;
	}
	
	public T redukj(Redukcija<T> redukcija) {
		T akumulator = null;
		if(this.elememnti.size() > 0) {
			akumulator = this.elememnti.get(0);
			
			for(int i = 1; i < this.elememnti.size(); i++) {
				akumulator = redukcija.redukuj(akumulator, this.elememnti.get(i));
			}
		}
		
		return akumulator;
	}
}
