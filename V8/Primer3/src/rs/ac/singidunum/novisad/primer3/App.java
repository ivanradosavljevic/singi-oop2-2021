package rs.ac.singidunum.novisad.primer3;

import rs.ac.singidunum.novisad.primer3.model.GenerickaKolekcija;

public class App {

	public static void main(String[] args) {
		GenerickaKolekcija<Integer> kolekcija1 = new GenerickaKolekcija<Integer>();
		kolekcija1.dodajElement(1);
		kolekcija1.dodajElement(2);
		kolekcija1.dodajElement(3);
		System.out.println(kolekcija1.filtriraj(v -> v > 1));
		System.out.println(kolekcija1.mapiraj(v -> v * 2));
		System.out.println(kolekcija1.redukj((a, v) -> a + v));

		GenerickaKolekcija<String> kolekcija2 = new GenerickaKolekcija<String>();
		kolekcija2.dodajElement("A");
		kolekcija2.dodajElement("BC");
		kolekcija2.dodajElement("D");
		System.out.println(kolekcija2.filtriraj(v -> v.length() > 1));
		System.out.println(kolekcija2.mapiraj(v -> v + "_"));
		System.out.println(kolekcija2.redukj((a, v) -> a + v));

		GenerickaKolekcija<String> kolekcija3 = new GenerickaKolekcija<String>();
		kolekcija3.dodajElement("AGF");
		kolekcija3.dodajElement("BC");
		kolekcija3.dodajElement("D");
		System.out.println(
				kolekcija3.filtriraj(v -> v.length() > 1).mapiraj(v -> v.toLowerCase()).redukj((a, v) -> a + v));
	}

}
