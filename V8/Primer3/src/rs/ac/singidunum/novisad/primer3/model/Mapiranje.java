package rs.ac.singidunum.novisad.primer3.model;

public interface Mapiranje<T> {
	T mapiraj(T o);
}
