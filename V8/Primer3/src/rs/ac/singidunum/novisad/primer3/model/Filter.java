package rs.ac.singidunum.novisad.primer3.model;

public interface Filter<T> {
	boolean filtriraj(T o);
}
