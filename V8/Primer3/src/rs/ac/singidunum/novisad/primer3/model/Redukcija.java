package rs.ac.singidunum.novisad.primer3.model;

public interface Redukcija<T> {
	T redukuj(T o1, T o2);
}
