#include "../headers/Predmet.hpp"

Predmet::Predmet() {

}

Predmet::Predmet(string naziv, string sifra) : naziv(naziv), sifra(sifra) {

}

void Predmet::dodajStudenta(Student *student, bool rekurzivno) {
	studenti.push_back(student);
	if(rekurzivno) {
		student->dodajPredmet(this, false);
	}
}
void Predmet::ukloniStudenta(Student *student, bool rekurzivno) {
	bool found = false;
	size_t i = 0;
	for(; i < predavaci.size(); i++) {
		if(studenti[i] == student) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	if(rekurzivno) {
		studenti[i]->ukloniPredmet(this, false);
	}

	studenti.erase(studenti.begin()+i);
}
void Predmet::dodajPredavaca(Profesor *profesor, bool rekurzivno) {
	predavaci.push_back(profesor);
	if(rekurzivno) {
		profesor->dodajPredmet(this, false);
	}
}
void Predmet::ukloniProfesora(Profesor *profesor, bool rekurzivno) {
	bool found = false;
	size_t i = 0;
	for(; i < predavaci.size(); i++) {
		if(predavaci[i] == profesor) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	if(rekurzivno) {
		predavaci[i]->ukloniPredmet(this, false);
	}

	predavaci.erase(predavaci.begin()+i);
}
void Predmet::dodajProveruZnanja(ProveraZnanja *proveraZnanja) {
	provere.push_back(proveraZnanja);
}
void Predmet::ukloniProveruZnanja(ProveraZnanja *proveraZnanja) {
	bool found = false;
	size_t i = 0;
	for(; i < provere.size(); i++) {
		if(provere[i] == proveraZnanja) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	provere.erase(provere.begin()+i);
}

Predmet::~Predmet() {

}
