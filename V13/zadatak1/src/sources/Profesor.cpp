#include "../headers/Profesor.hpp"

Profesor::Profesor() : Osoba() {

}

Profesor::Profesor(string ime, string prezime, string jmbg) : Osoba(ime, prezime, jmbg) {

}

void Profesor::dodajPredmet(Predmet *predmet, bool rekurzivno) {
	predaje.push_back(predmet);
	if(rekurzivno) {
		predmet->dodajPredavaca(this, false);
	}
}

void Profesor::ukloniPredmet(Predmet *predmet, bool rekurzivno) {
	bool found = false;
	size_t i = 0;
	for(; i < predaje.size(); i++) {
		if(predaje[i] == predmet) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	if(rekurzivno) {
		predaje[i]->ukloniProfesora(this, false);
	}

	predaje.erase(predaje.begin()+i);
}

void Profesor::detalji() {
	cout << ime << " " << prezime << " " << jmbg << " ";
}

Profesor::~Profesor() {

}
