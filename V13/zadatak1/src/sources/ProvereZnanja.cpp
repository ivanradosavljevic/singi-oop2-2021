#include "../headers/ProveraZnanja.hpp"

ProveraZnanja::ProveraZnanja(Predmet *predmet, TipProvere tipProvere, double ocena) : predmet(predmet), tipProvere(tipProvere), ocena(ocena) {

}

string ProveraZnanja::sifraPredmeta(){
	return predmet->getSifraPredmeta();
}

ProveraZnanja::~ProveraZnanja() {

}
