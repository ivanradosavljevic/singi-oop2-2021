#ifndef HEADERS_OSOBA_HPP_
#define HEADERS_OSOBA_HPP_

#include <string>
using namespace std;

class Osoba {
protected:
	string ime;
	string prezime;
	string jmbg;
	
	Osoba();
	Osoba(string ime, string prezime, string jmbg);
	virtual void detalji() = 0;
	virtual ~Osoba();
};



#endif /* HEADERS_OSOBA_HPP_ */
