#ifndef HEADERS_PREDMET_HPP_
#define HEADERS_PREDMET_HPP_

#include "../headers/Student.hpp"
#include "../headers/Profesor.hpp"
#include "../headers/ProveraZnanja.hpp"
#include <vector>
using namespace std;

class Student;
class Profesor;
class ProveraZnanja;

class Predmet {
private:
	string naziv;
	string sifra;
	vector<Student*> studenti;
	vector<Profesor*> predavaci;
	vector<ProveraZnanja*> provere;
public:
	Predmet();
	Predmet(string naziv, string sifra);
	void dodajStudenta(Student *student, bool rekurzivno=true);
	void ukloniStudenta(Student *student, bool rekurzivno = true);
	void dodajPredavaca(Profesor *profesor, bool rekurzivno=true);
	void ukloniProfesora(Profesor *profesor, bool rekurzivno = true);
	void dodajProveruZnanja(ProveraZnanja *proveraZnanja);
	void ukloniProveruZnanja(ProveraZnanja *proveraZnanja);

	string getSifraPredmeta() {
		return sifra;
	}
	virtual ~Predmet();
};



#endif /* HEADERS_PREDMET_HPP_ */
