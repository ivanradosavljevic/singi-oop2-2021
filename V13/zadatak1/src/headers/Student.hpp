#ifndef HEADERS_STUDENT_HPP_
#define HEADERS_STUDENT_HPP_

#include "../headers/Osoba.hpp"
#include "../headers/Predmet.hpp"
#include <iostream>
#include <vector>

using namespace std;

class Predmet;

class Student : public Osoba {
private:
	string brojIndeksa;
	vector<Predmet*> pohadjanja;
public:
	Student();
	Student(string ime, string prezime, string jmbg, string brojIndeksa);
	void dodajPredmet(Predmet *predmet, bool rekurzivno=true);
	void ukloniPredmet(Predmet *predmet, bool rekurzivno=true);
	virtual void detalji();
	virtual ~Student();
};



#endif /* HEADERS_STUDENT_HPP_ */
