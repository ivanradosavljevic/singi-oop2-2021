#ifndef HEADERS_PROVERAZNANJA_HPP_
#define HEADERS_PROVERAZNANJA_HPP_

#include "../headers/Predmet.hpp"

class predmet;

enum TipProvere{PREDISPITNA_OBAVEZA, ISPIT};

class ProveraZnanja {
private:
	Predmet *predmet;
	TipProvere tipProvere;
	double ocena;
public:
	ProveraZnanja(Predmet *predmet, TipProvere tipProvere, double ocena);
	string sifraPredmeta();
	virtual ~ProveraZnanja();
};



#endif /* HEADERS_PROVERAZNANJA_HPP_ */
