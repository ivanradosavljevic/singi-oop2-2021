package rs.ac.singidunum.novisad.zadatak2.model;

public class Filozof implements Runnable {
	private Viljuska leva;
	private Viljuska desna;

	public Filozof() {
		super();
	}

	public Filozof(Viljuska leva, Viljuska desna) {
		super();
		this.leva = leva;
		this.desna = desna;
	}

	public Viljuska getLeva() {
		return leva;
	}

	public void setLeva(Viljuska leva) {
		this.leva = leva;
	}

	public Viljuska getDesna() {
		return desna;
	}

	public void setDesna(Viljuska desna) {
		this.desna = desna;
	}

	@Override
	public void run() {
		try {
			while (true) {
				System.out.println(Thread.currentThread().getName() + ": razmislja.");
				Thread.sleep((long) (Math.random() * 25));
				synchronized (leva) {
					Thread.sleep((long) (Math.random() * 250));
					System.out.println(Thread.currentThread().getName() + ": uzeo levu viljusku.");
					synchronized (desna) {
						Thread.sleep((long) (Math.random() * 25));
						System.out.println(Thread.currentThread().getName() + ": uzeo desnu viljusku.");
						
						System.out.println(Thread.currentThread().getName() + ": jede.");
						Thread.sleep((long) (Math.random() * 25));
						
						Thread.sleep((long) (Math.random() * 25));
						System.out.println(Thread.currentThread().getName() + ": vratio desnu viljusku.");
					}
				}
				Thread.sleep((long) (Math.random() * 25));
				System.out.println(Thread.currentThread().getName() + ": vratio levu viljusku.");
				
				System.out.println(Thread.currentThread().getName() + ": zavrsio jelo.");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
