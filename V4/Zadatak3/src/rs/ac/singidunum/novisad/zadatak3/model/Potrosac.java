package rs.ac.singidunum.novisad.zadatak3.model;

public class Potrosac implements Runnable {
	private Kolekcija kolekcija;

	public Potrosac() {
		super();
	}

	public Potrosac(Kolekcija kolekcija) {
		super();
		this.kolekcija = kolekcija;
	}

	public Kolekcija getKolekcija() {
		return kolekcija;
	}

	public void setKolekcija(Kolekcija kolekcija) {
		this.kolekcija = kolekcija;
	}

	@Override
	public void run() {
		while (true) {
			try {
//				Thread.sleep((long)(Math.random()*100));
				System.out.println(Thread.currentThread().getName() + ": " + this.kolekcija.dobavi());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
