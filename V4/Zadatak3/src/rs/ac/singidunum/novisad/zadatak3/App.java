package rs.ac.singidunum.novisad.zadatak3;

import rs.ac.singidunum.novisad.zadatak3.model.Kolekcija;
import rs.ac.singidunum.novisad.zadatak3.model.Potrosac;
import rs.ac.singidunum.novisad.zadatak3.model.Proizvodjac;

public class App {

	public static void main(String[] args) throws InterruptedException {
		Kolekcija kolekcija = new Kolekcija(10);
		Thread potrosac1 = new Thread(new Potrosac(kolekcija));
		Thread potrosac2 = new Thread(new Potrosac(kolekcija));
		Thread proizvodjac1 = new Thread(new Proizvodjac(kolekcija));
		
		potrosac1.start();
		potrosac2.start();
		proizvodjac1.start();
	}

}
