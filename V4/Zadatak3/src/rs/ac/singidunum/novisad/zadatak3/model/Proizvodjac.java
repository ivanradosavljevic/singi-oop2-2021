package rs.ac.singidunum.novisad.zadatak3.model;

public class Proizvodjac implements Runnable {
	private Kolekcija kolekcija;

	public Proizvodjac() {
		super();
	}

	public Proizvodjac(Kolekcija kolekcija) {
		super();
		this.kolekcija = kolekcija;
	}

	public Kolekcija getKolekcija() {
		return kolekcija;
	}

	public void setKolekcija(Kolekcija kolekcija) {
		this.kolekcija = kolekcija;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep((long)(Math.random()*1000));
				this.kolekcija.dodaj((int) (Math.random() * 100));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
