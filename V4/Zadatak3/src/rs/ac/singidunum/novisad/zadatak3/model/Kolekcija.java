package rs.ac.singidunum.novisad.zadatak3.model;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Kolekcija {
	private int maksimalniBrojElemenata = 5;
	private ArrayList<Integer> brojevi = new ArrayList<Integer>();
	private ReentrantLock lock = new ReentrantLock();
	private final Condition nepunNiz;
	private final Condition neprazanNiz;

	public Kolekcija() {
		super();
		nepunNiz = this.lock.newCondition();
		neprazanNiz = this.lock.newCondition();
	}

	public Kolekcija(int maksimalniBrojElemenata) {
		this();
		this.maksimalniBrojElemenata = maksimalniBrojElemenata;
	}

	public int getMaksimalniBrojElemenata() {
		return maksimalniBrojElemenata;
	}

	public void setMaksimalniBrojElemenata(int maksimalniBrojElemenata) {
		this.maksimalniBrojElemenata = maksimalniBrojElemenata;
	}

	public ArrayList<Integer> getBrojevi() {
		return brojevi;
	}

	public void setBrojevi(ArrayList<Integer> brojevi) {
		this.brojevi = brojevi;
	}

	public void dodaj(Integer broj) throws InterruptedException {
		this.lock.lockInterruptibly();
		while (this.brojevi.size() >= this.maksimalniBrojElemenata) {
			nepunNiz.await();
		}
		this.brojevi.add(broj);
		neprazanNiz.signal();
		this.lock.unlock();

	}

	public Integer dobavi() throws InterruptedException {
		this.lock.lockInterruptibly();
		while (this.brojevi.size() == 0) {
			neprazanNiz.await();
		}
		Integer broj = this.brojevi.remove(0);
		nepunNiz.signal();
		this.lock.unlock();
		return broj;
	}
}
