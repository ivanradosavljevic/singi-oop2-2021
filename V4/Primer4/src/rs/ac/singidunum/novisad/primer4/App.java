package rs.ac.singidunum.novisad.primer4;

import java.util.ArrayList;

import rs.ac.singidunum.novisad.primer4.model.Banka;
import rs.ac.singidunum.novisad.primer4.model.Klijent;

public class App {

	public static void main(String[] args) throws InterruptedException {
		Banka banka = new Banka(10000);
		ArrayList<Thread> klijenti = new ArrayList<Thread>();
		for (int i = 0; i < 10; i++) {
			klijenti.add(new Thread(new Klijent(banka)));
		}
		
		System.out.println("Sredstva na pocetku: " + banka.getSredstva());
		for (Thread k : klijenti) {
			k.start();
		}

		for (Thread k : klijenti) {
			k.join();
		}
		System.out.println("Sredstva na kraju: " + banka.getSredstva());
	}

}
