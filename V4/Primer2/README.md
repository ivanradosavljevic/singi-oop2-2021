# Primer 2

1. Napisati klasu Banka. Banka sadrži atribut sredstva koji predstavlja količinu raspoloživih sredstava u banci. Banka sadrži i metode uplati i ispalti, ove metode vrše uplatu, odnosno isplatu sredstava u zadatoj visini. 
2. Napisati klasu Klijent. Omogućiti da svaki klijent u odvojenoj niti vrši uplate i isplate. Prilikom svake uplate i isplate ispisati stanje u banci za klijenta koji je vršio uplatu, odnosno isplatu.
3. U main metodi instancirati banku i nekoliko klijenata. Pokrenuti izvršavanje uplata i isplata.