#include <iostream>
using namespace std;

class Pravougaonik {
private:
	double a;
	double b;
public:
	Pravougaonik(double a, double b) : a(a), b(b) {}
	double povrsina() {
		return a*b;
	}
};

int main() {
	Pravougaonik p(10.2, 18.5);
	cout << p.povrsina() << endl;
	return 0;
}
