#include "../headers/pravougaonik.h"

Pravougaonik::Pravougaonik(double a, double b) : Oblik(), a(a), b(b) {}

double Pravougaonik::povrsina() {
	return a*b;
}
