#ifndef HEADERS_KRUG_H_
#define HEADERS_KRUG_H_

#define _USE_MATH_DEFINES

#include "oblik.h"
#include <cmath>

class Krug : public Oblik {
private:
	double r;
public:
	Krug(double r);
	double povrsina();
};


#endif /* HEADERS_KRUG_H_ */
