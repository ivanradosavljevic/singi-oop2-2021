#ifndef HEADERS_PRAVOUGAONIK_H_
#define HEADERS_PRAVOUGAONIK_H_

class Pravougaonik {
private:
	double a;
	double b;
public:
	Pravougaonik(double a, double b) : a(a), b(b) {}
	double povrsina() {
		return a*b;
	}
};

#endif
