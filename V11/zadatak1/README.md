# Zadatak 1
1. Napraviti apstraktnu klasu Osoba koja sadrži atribute ime i prezime i metodu predstaviSe koja ispisuje ime i prezime osobe.
2. Napraviti klasu radnik koja nasleđuje klasu Osoba i dodaje atribute sifraZaposlenog i visinaPlate. Ova klasa redefiniše metodu predstavi se tako da dodatno ispisuje i podatke o šifri zaposlenog i visini plate.
3. Dodati klase Direktor i Menadžer. Direktor ima atribut bonus koji platu uvećava za zadati bonus. Menadžer ima dodatni atribut podređeni koji sadrži sve radnike koji su podređeni menadžeru. Prilikom ispisa podataka o direktoru i menadžeru ispisati sve njihove dodatne podatke.
4. Napisati klasu preduzeće koja sadrži kolekciju radnika. Za preduzeće dodati metode za dodavanje i uklanjanje radnika. Omogućiti ispis svih zaposlenih u preduzeću.
5. Instancirati klase i testirati ispravnost rešenja.