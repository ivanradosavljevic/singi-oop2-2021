#include "../headers/krug.h"

Krug::Krug(double r) : Oblik(), r(r) {}

double Krug::povrsina() {
	return r*r*M_PI;
}
