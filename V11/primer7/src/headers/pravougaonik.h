#ifndef HEADERS_PRAVOUGAONIK_H_
#define HEADERS_PRAVOUGAONIK_H_

#include "oblik.h"

class Pravougaonik : public Oblik {
private:
	double a;
	double b;
public:
	Pravougaonik(double a, double b);
	double povrsina();
};

#endif /* HEADERS_PRAVOUGAONIK_H_ */
