package rs.ac.singidunum.novisad.zadatak1.model;

public abstract class ImenovaniElement {
	private String naziv;

	public ImenovaniElement() {
	}

	public ImenovaniElement(String naziv) {
		super();
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
