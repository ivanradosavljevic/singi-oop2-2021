package rs.ac.singidunum.novisad.zadatak1.model;

public class Parametar extends ImenovaniElement {
	private String tip;

	public Parametar() {
		super();
	}

	public Parametar(String naziv, String tip) {
		super(naziv);
		this.tip = tip;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}
}
