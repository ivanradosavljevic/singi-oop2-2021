# Zadatak 2

1. Napisati klase i interefejse koji opisuju graf izračunavanja. Graf izračunavanje je graf koji je sačinjen od čvorova koji opisuju operande i operacije. Ograničiti se na to da su operandi brojevi u dvostrukoj preciznosti, dok operacije mogu biti unarne, binarne ili n-arne. Operacije i operandi opciono mogu biti imenovani, npr.: "Promenljiva A", "Suma svih brojeva".
2. Svi čvorovi koji mogu da se evaluiraju treba da podržavaju metodu eval, ova metoda ne prima ni jedan parametar a kao rezultat vraća novi čvor koji može biti evaluiran. Na primer čvor za sabiranje je čvor koji može da se evaluira, rezultat evaluiranja čvora za sumiranje je čvor koji u sebi sadrži vrednost rezultata sabiranja.
3. Za sve čvorove omogućiti ispisivanje njihovog internog stanja, odnosno vrednosti atributa definisanih u čvoru.
4. Testirati rešenje instanciranjem nekoliko čvorova, njihovim uvezivanjem i pokretanjem metode za evaluiranje grafa i ispisom rezultata.