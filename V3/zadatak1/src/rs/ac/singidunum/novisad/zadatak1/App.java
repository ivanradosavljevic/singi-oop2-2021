package rs.ac.singidunum.novisad.zadatak1;

import rs.ac.singidunum.novisad.zadatak1.model.Kanal;
import rs.ac.singidunum.novisad.zadatak1.model.Korisnik;
import rs.ac.singidunum.novisad.zadatak1.model.PrivatniKanal;

public class App {
	public static void main(String[] args) {
		Korisnik korisnik1 = new Korisnik("pera", "pera@localhost");
		Korisnik korisnik2 = new Korisnik("mika", "mika@localhost");
		Korisnik korisnik3 = new Korisnik("jovan", "jovan@localhost");
		Korisnik korisnik4 = new Korisnik("goran", "goran@localhost");

		Kanal kanal1 = new Kanal("Kanal1");
		Kanal kanal2 = new Kanal("kanal2");

		kanal1.dodajKorisnika(korisnik1);
		kanal1.dodajKorisnika(korisnik2);
		kanal1.dodajKorisnika(korisnik3);

		kanal2.dodajKorisnika(korisnik1);
		kanal2.dodajKorisnika(korisnik4);

		kanal1.dodajPoruku(korisnik1, "Pozdrav");
		kanal1.dodajPoruku(korisnik2, "Zdravo!");

		kanal2.dodajPoruku(korisnik1, "test 1");
		kanal2.dodajPoruku(korisnik4, "test 2");

		kanal1.ispisi();
		kanal2.ispisi();

		korisnik1.posaljiPoruku(korisnik3, "Zdravo!");
		korisnik3.posaljiPoruku(korisnik1, "Pozdrav!");

		for (PrivatniKanal pk : korisnik1.getPrivatniKanali()) {
			pk.ispisi();
		}
		for (PrivatniKanal pk : korisnik3.getPrivatniKanali()) {
			pk.ispisi();
		}
	}
}
