package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;
import java.util.List;

public class Korisnik implements Ispisivo {
	private String korisnickoIme;
	private String email;
	private List<PrivatniKanal> privatniKanali = new ArrayList<PrivatniKanal>();

	public Korisnik() {
		super();
	}

	public Korisnik(String korisnickoIme, String email) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.email = email;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<PrivatniKanal> getPrivatniKanali() {
		return privatniKanali;
	}

	public void setPrivatniKanali(List<PrivatniKanal> privatniKanali) {
		this.privatniKanali = privatniKanali;
	}

	@Override
	public void ispisi() {
		System.out.print(this.korisnickoIme);
	}
	
	public void posaljiPoruku(Korisnik primalac, String sadrzaj) {
		PrivatniKanal kanal = null;
		for(PrivatniKanal pk : privatniKanali) {
			if(pk.getKorisnici().contains(primalac)) {
				kanal = pk;
			}
		}
		if(kanal == null) {
			kanal = new PrivatniKanal(this, primalac);
			
			this.privatniKanali.add(kanal);
			primalac.privatniKanali.add(kanal);
		}
		
		kanal.dodajPoruku(this, sadrzaj);
	}
}
