package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.Date;

public class Poruka implements Ispisivo {

	private Korisnik autor;
	private Date datumIVreme;
	private String sadrzaj;

	public Poruka() {
		super();
	}

	public Poruka(Date datumIVreme, Korisnik autor, String sadrzaj) {
		super();
		this.datumIVreme = datumIVreme;
		this.autor = autor;
		this.sadrzaj = sadrzaj;
	}

	public Korisnik getAutor() {
		return autor;
	}

	public void setAutor(Korisnik autor) {
		this.autor = autor;
	}

	public Date getDatumIVreme() {
		return datumIVreme;
	}

	public void setDatumIVreme(Date datumIVreme) {
		this.datumIVreme = datumIVreme;
	}

	public String getSadrzaj() {
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}

	@Override
	public void ispisi() {
		System.out.print(this.datumIVreme + " - ");
		this.autor.ispisi();
		System.out.print(": " + this.sadrzaj);

	}
}
