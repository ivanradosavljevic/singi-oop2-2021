package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Kanal implements Ispisivo {
	private String naziv;
	protected List<Korisnik> korisnici = new ArrayList<Korisnik>();
	protected List<Poruka> poruke = new ArrayList<Poruka>();

	public Kanal() {
		super();
	}

	public Kanal(String naziv) {
		super();
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(List<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}

	public List<Poruka> getPoruke() {
		return poruke;
	}

	public void setPoruke(List<Poruka> poruke) {
		this.poruke = poruke;
	}

	public void dodajKorisnika(Korisnik korisnik) {
		this.korisnici.add(korisnik);
	}
	
	public Korisnik ukloniKorisnika(int indeks) {
		return this.korisnici.remove(indeks);
	}
	
	public void dodajPoruku(Poruka poruka) {
		if (!korisnici.contains(poruka.getAutor())) {
			throw new IllegalArgumentException("Zadati korisnik nije u kanalu");
		}
		this.poruke.add(poruka);
	}

	public void dodajPoruku(Korisnik autor, String sadrzaj) {
		if (!korisnici.contains(autor)) {
			throw new IllegalArgumentException("Zadati korisnik nije u kanalu");
		}
		this.poruke.add(new Poruka(new Date(), autor, sadrzaj));

	}

	@Override
	public void ispisi() {
		System.out.println("Sadrzaj kanala " + this.naziv + ":");
		for(Poruka p : this.poruke) {
			p.ispisi();
			System.out.println();
		}
		System.out.println("------------------");
	}
}
