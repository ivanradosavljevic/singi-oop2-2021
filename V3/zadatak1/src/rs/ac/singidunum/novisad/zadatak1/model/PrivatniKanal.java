package rs.ac.singidunum.novisad.zadatak1.model;

public class PrivatniKanal extends Kanal {
	public PrivatniKanal(Korisnik korisnik1, Korisnik korisnik2) {
		super("Privatni kanal [" + korisnik1.getKorisnickoIme() + " - " + korisnik2.getKorisnickoIme() + "]");
		this.dodajKorisnika(korisnik1);
		this.dodajKorisnika(korisnik2);
	}

	public void dodajKorisnika(Korisnik korisnik) {
		if (this.korisnici.size() < 2) {
			this.korisnici.add(korisnik);
		} else {
			throw new RuntimeException("Privatni kanal ne moze sadrzati vise od dva korisnika!");
		}
	}

	public Korisnik ukloniKorisnika(int indeks) {
		return null;
	}
}
