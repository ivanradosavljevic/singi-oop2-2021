package rs.ac.singidunum.novisad.primer5;

import rs.ac.singidunum.novisad.primer5.ui.MainWindow;

public class App {

	public static void main(String[] args) {
		MainWindow mainWindow = new MainWindow();
		mainWindow.init();
	}

}
