package rs.ac.singidunum.novisad.primer5.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToolBar;

import rs.ac.singidunum.novisad.primer5.model.RadnikTableModel;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = -3195317199343595907L;

	public MainWindow() throws HeadlessException {
		super();
	}

	public MainWindow(GraphicsConfiguration gc) {
		super(gc);
	}

	public MainWindow(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

	public MainWindow(String title) throws HeadlessException {
		super(title);
	}

	public void init() {
		this.setTitle("Glavni prozor");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenuItem = new JMenuItem("Exit");

		JToolBar toolbar = new JToolBar("Toolbar");
		JButton button = new JButton("Exit");
		button.addActionListener(e -> System.exit(0));

		toolbar.add(button);

		exitMenuItem.addActionListener(e -> {
			System.exit(0);
		});

		fileMenu.add(exitMenuItem);
		menuBar.add(fileMenu);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		RadnikForm radnikForma = new RadnikForm();
		radnikForma.setMinimumSize(new Dimension(300, 200));

		RadnikTableModel radnikTableModel = new RadnikTableModel();
		JTable table = new JTable(radnikTableModel);
		table.setFillsViewportHeight(true);
		
		JScrollPane scrollPane = new JScrollPane(table);
		

		splitPane.add(radnikForma);
		splitPane.add(scrollPane);

		radnikForma.addActionListener(e -> {
			radnikTableModel.addRow(radnikForma.getFormData());
		});

		table.getSelectionModel().addListSelectionListener(e -> {
			if(!e.getValueIsAdjusting()) {
				System.out.println(radnikTableModel.getRow(table.getSelectedRow()));
			}
		});

		this.setJMenuBar(menuBar);

		this.getContentPane().add(toolbar, BorderLayout.PAGE_START);
		this.getContentPane().add(splitPane, BorderLayout.CENTER);
		this.setVisible(true);
	}
}
