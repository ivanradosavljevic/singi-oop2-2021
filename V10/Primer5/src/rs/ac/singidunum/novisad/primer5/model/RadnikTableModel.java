package rs.ac.singidunum.novisad.primer5.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class RadnikTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 8597992305424395241L;
	private ArrayList<Radnik> radnici = new ArrayList<Radnik>();
	@Override
	public int getRowCount() {
		return this.radnici.size();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex == 0) {
			return this.radnici.get(rowIndex).getIme();
		}
		if(columnIndex == 1) {
			return this.radnici.get(rowIndex).getPrezime();
		}
		if(columnIndex == 2) {
			return this.radnici.get(rowIndex).getVisinaPlate();
		}
		if(columnIndex == 3) {
			return this.radnici.get(rowIndex).getZanimanje().getNaziv();
		}
		return null;
	}
	
	@Override
	public String getColumnName(int column) {
		if(column == 0) {
			return "Ime";
		}
		if(column == 1) {
			return "Prezime";
		}
		if(column == 2) {
			return "Visina plate";
		}
		if(column == 3) {
			return "Zvanje";
		}
		return super.getColumnName(column);
	}
	
	public Radnik getRow(int rowIndeks) {
		return this.radnici.get(rowIndeks);
	}
	
	public void addRow(Radnik radnik) {
		this.radnici.add(radnik);
		this.fireTableRowsInserted(this.radnici.size()-1, this.radnici.size());
	}
}
