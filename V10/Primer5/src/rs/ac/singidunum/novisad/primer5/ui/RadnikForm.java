package rs.ac.singidunum.novisad.primer5.ui;

import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;

import rs.ac.singidunum.novisad.primer5.model.Radnik;
import rs.ac.singidunum.novisad.primer5.model.Zanimanje;

public class RadnikForm extends JPanel {
	private static final long serialVersionUID = 6984416751357269078L;
	protected JTextField nameInput = new JTextField(20);
	protected JTextField surnameInput = new JTextField(20);
	protected SpringLayout springLayout = new SpringLayout();
	protected JSpinner paySpinner = new JSpinner(new SpinnerNumberModel(0, 0, Double.MAX_VALUE, 0.1));
	protected JComboBox<Zanimanje> occupationComboBox = new JComboBox<>(new DefaultComboBoxModel<>());
	protected JButton button = new JButton("Dodaj");
	
	public RadnikForm() {
		super();
		this.setLayout(this.springLayout);
		this.init();
	}
	
	private void init() {
		JLabel nameLabel = new JLabel("Ime: ");
		JLabel surnameLabel = new JLabel("Prezime: ");
		JLabel payLabel = new JLabel("Visina plate: ");
		JLabel occupationLabel = new JLabel("Zanimanje: ");
		
		JButton addOccupationButton = new JButton("Dodaj zanimanje");
		
		addOccupationButton.addActionListener(e->{
			OccupationDialog occupationDialog = new OccupationDialog();
			occupationDialog.setVisible(true);
			this.occupationComboBox.addItem(occupationDialog.getZanimanje());
		});
		
		this.occupationComboBox.addItem(new Zanimanje("Racunovodja"));
		this.occupationComboBox.addItem(new Zanimanje("Prodavac"));
		
		this.add(nameLabel);
		this.add(nameInput);
		this.add(surnameLabel);
		this.add(surnameInput);
		this.add(payLabel);
		this.add(paySpinner);
		this.add(occupationLabel);
		this.add(occupationComboBox);
		this.add(addOccupationButton);
		this.add(button);
		
		this.springLayout.putConstraint(SpringLayout.EAST, nameLabel, 100, SpringLayout.WEST, this);
		
		this.springLayout.putConstraint(SpringLayout.NORTH, nameInput, 10, SpringLayout.NORTH, this);
		this.springLayout.putConstraint(SpringLayout.WEST, nameInput, 10, SpringLayout.EAST, nameLabel);
		this.springLayout.putConstraint(SpringLayout.EAST, nameInput, -10, SpringLayout.EAST, this);
		
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, nameLabel, 0, SpringLayout.VERTICAL_CENTER, nameInput);
		
		this.springLayout.putConstraint(SpringLayout.EAST, surnameLabel, 100, SpringLayout.WEST, this);
		
		this.springLayout.putConstraint(SpringLayout.NORTH, surnameInput, 10, SpringLayout.SOUTH, nameInput);
		this.springLayout.putConstraint(SpringLayout.WEST, surnameInput, 10, SpringLayout.EAST, surnameLabel);
		this.springLayout.putConstraint(SpringLayout.EAST, surnameInput, -10, SpringLayout.EAST, this);
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, surnameLabel, 0, SpringLayout.VERTICAL_CENTER, surnameInput);
		
		this.springLayout.putConstraint(SpringLayout.EAST, payLabel, 100, SpringLayout.WEST, this);
		this.springLayout.putConstraint(SpringLayout.NORTH, paySpinner, 10, SpringLayout.SOUTH, surnameInput);
		this.springLayout.putConstraint(SpringLayout.WEST, paySpinner, 10, SpringLayout.EAST, payLabel);
		this.springLayout.putConstraint(SpringLayout.EAST, paySpinner, -10, SpringLayout.EAST, this);
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, payLabel, 0, SpringLayout.VERTICAL_CENTER, paySpinner);
		
		this.springLayout.putConstraint(SpringLayout.EAST, occupationLabel, 100, SpringLayout.WEST, this);
		this.springLayout.putConstraint(SpringLayout.NORTH, occupationComboBox, 10, SpringLayout.SOUTH, paySpinner);
		this.springLayout.putConstraint(SpringLayout.WEST, occupationComboBox, 10, SpringLayout.EAST, occupationLabel);
		this.springLayout.putConstraint(SpringLayout.EAST, occupationComboBox, -200, SpringLayout.EAST, this);
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, occupationLabel, 0, SpringLayout.VERTICAL_CENTER, occupationComboBox);
		
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, addOccupationButton, 0, SpringLayout.VERTICAL_CENTER, occupationComboBox);
		this.springLayout.putConstraint(SpringLayout.WEST, addOccupationButton, 10, SpringLayout.EAST, occupationComboBox);
		this.springLayout.putConstraint(SpringLayout.EAST, addOccupationButton, -10, SpringLayout.EAST, this);
		
		this.springLayout.putConstraint(SpringLayout.NORTH, button, 10, SpringLayout.SOUTH, occupationComboBox);
		this.springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, button, 0, SpringLayout.HORIZONTAL_CENTER, this);
	}
	
	public Radnik getFormData() {
		return new Radnik(nameInput.getText(), surnameInput.getText(), (double)paySpinner.getValue(), (Zanimanje)occupationComboBox.getSelectedItem());
	}
	
	public void addActionListener(ActionListener listener) {
		this.button.addActionListener(listener);
	}
}
