package rs.ac.singidunum.novisad.primer1;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;

import java.awt.*;

public class App {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Glavni prozor");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenuItem = new JMenuItem("Exit");

		JToolBar toolbar = new JToolBar("Toolbar");
		JButton button = new JButton("Exit");
		button.addActionListener(e -> System.exit(0));

		toolbar.add(button);

		exitMenuItem.addActionListener(e -> {
			System.exit(0);
		});

		fileMenu.add(exitMenuItem);
		menuBar.add(fileMenu);

		JLabel label = new JLabel("Primer prozora!");

		frame.setJMenuBar(menuBar);

		frame.getContentPane().add(toolbar, BorderLayout.PAGE_START);
		frame.getContentPane().add(label, BorderLayout.CENTER);
		frame.setVisible(true);
	}

}
