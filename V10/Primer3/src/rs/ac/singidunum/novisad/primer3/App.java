package rs.ac.singidunum.novisad.primer3;

import rs.ac.singidunum.novisad.primer3.ui.MainWindow;

public class App {

	public static void main(String[] args) {
		MainWindow mainWindow = new MainWindow();
		mainWindow.init();
	}

}
