# Primer 4
1. Dodati klasu zanimanje koja sadrži atribut naziv. Proširiti klasu Radnik uvođenjem atributa visinaPlate i zanimanje.
2. Prepraviti formu tako da se omogući izbor visine plate. Visina plate ima minimalnu vrednost 0. Zanimanje se bira iz skupa ponuđenih vrednosti. Vrednosti ponuditi preko padajuće liste.
3. Pored padajuće liste dodati dugme za dodavanje stavki u padajuću listu. Stavke se dodaju preko dijaloga koji u sebi sadrži polje za unos naziva stavke.